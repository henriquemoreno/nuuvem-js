# Where is each Questions

I've used ES6 just for syntax "compatibility" with Java, except with Debbuging questions.

## Prime Numbers and Balanced String

They were created as `src` in files:

* `src/prime`
* `src/balanced.js`

And each unit test is at `__tests__`:

* `__tests__\prime.js`
* `__tests__\balanced.js`

## Debugging existing code

### Greetings

This correct answer also have unit test (at `__test__\greet.js`) and source (at `src\greet.js`).

The reason to not run correctly is:

>    "if (person == { name: 'amy' })" cannot be using traditional compation according:
>    https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness#A_model_for_understanding_equality_comparisons

### Bark

The source is at `bark.js`.

Using:

```js
Dog.bark = function () {
  console.log(this.name + ' says woof')
}
```
It just create function to the "Object" not in it's definition. It was solved using prototype, in that case you can use 2 ways:

* Creating internally
* Adding to prototype

Creating intenally method is more readeable and encapsulated, but with prototype the method can  be override almost anywhere in the method, what could be dangerous.

### Running bark

Use:

`yarn run bark` or `npm run bark`

## Service Workers

At `web` directory run `yast install` or `npm install` and then `yast run start` or `npm run start`. Then acesss:

http://localhost:8081

Shutdown de server and try to reload again.

# Running Tests

Run:

`npm install` or `yarn install`

and then run:

`npm test` or `yarn test`






