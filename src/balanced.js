const tokens=[['{','}'],['(',')'],['[',']']];
const isBalanced = (inputStr) => {
    if (inputStr === null) { return true; }
    var expression = inputStr.split('');
    var stack = [];
    for (var i = 0; i < expression.length; i++) {
        const char = expression[i];
        if (isParanthesis(char)) {
            if (isOpenParenthesis(char)) {
                stack.push(char);
            } else {
                if (stack.length === 0) {
                    return false;
                }
                var top = stack.pop(); // pop off the top element from stack
                if (!matches(top, char)) {
                    return false;
                }
            }
        }
    }
    return stack.length === 0 ? true : false;;
};
const isParanthesis = (char) => {
    var str = '{}[]()';
    if (str.indexOf(char) > -1) {
        return true;
    } else {
        return false;
    }
}
const isOpenParenthesis = (parenthesisChar) => {
    for (var j = 0; j < tokens.length; j++) {
        if (tokens[j][0] === parenthesisChar) {
            return true;
        }
    }
    return false;
}
const matches = (topOfStack, closedParenthesis) => {
    for (var k = 0; k < tokens.length; k++) {
        if (tokens[k][0] === topOfStack &&
            tokens[k][1] === closedParenthesis) {
            return true;
        }
    }
    return false;
}
export { isBalanced };