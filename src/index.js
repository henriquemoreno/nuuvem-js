export {isPrime} from "./prime"
export {isBalanced} from "./balanced"
export {greet} from "./greet"
export {Dog} from "./bark"