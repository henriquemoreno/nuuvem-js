const greet = (person) => {
    /* 
    "if (person == { name: 'amy' })" cannot be using traditional compation according:
    https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness#A_model_for_understanding_equality_comparisons
    */
    if (person.name == 'amy') {
        return 'hey amy'
      } else {
        return 'hey arnold'
      }    
}
export {greet}