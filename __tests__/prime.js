import {isPrime} from "../src/index"
import each from 'jest-each'

describe('Testing prime number function', () => {
        each`
        input   |       expectedResult
        ${0}    |       ${false}
        ${1}    |       ${false}
        ${2}    |       ${true}
        ${3}    |       ${true}
        ${4}    |       ${false}
        ${17}   |       ${true}
        ${10000000000000}       | ${false}
        //`.test('isPrime($input)', ({ input, expectedResult }) => {
                expect(isPrime(input)).toBe(expectedResult);
        });

});