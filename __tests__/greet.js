import { greet } from "../src/index"
test("Greet", () => {
    expect(greet({ name: 'amy' })).toBe('hey amy')
});