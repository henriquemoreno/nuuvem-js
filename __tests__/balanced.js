import {isBalanced} from "../src/index"
import each from 'jest-each'
describe('Open and close parser test', () => {
    each`
    input                           |       expectedResult
    ${')('}                         |       ${false}
    ${'(()'}                        |       ${false}
    ${'()()'}                       |       ${true}
    ${'foo ( bar ( baz ) boo )'}    |       ${true}
    ${'foo ( bar ( baz )'}          |       ${false}
    ${'foo ( bar ) )'}              |       ${false}
    ${'(foo { bar (baz) [boo] })'}  |       ${true}
    ${'foo { bar { baz }'}          |       ${false}
    ${'foo { (bar [baz] } )'}       |       ${false}
    //`.test('isBalanced("$input")', ({ input, expectedResult }) => {
            expect(isBalanced(input)).toBe(expectedResult);
    })

})