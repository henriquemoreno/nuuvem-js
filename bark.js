function Dog(name) {
    this.name = name;
}

Dog.prototype.bark = function () {
    return this.name + ' says woof';
}

let fido = new Dog('fido')
console.log(fido.bark());